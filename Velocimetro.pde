//Sonido
import processing.sound.*;
SoundFile alerta;

//Variables
float x = 0;
int i;
int y = 0;
int t = 0;


void setup()
{
  size(800, 800);
  alerta = new SoundFile(this,"Sonido.mp3");
}
void draw()
{
  stroke(255, 0, 0);
  background(200);
  //FIGURAS
  strokeWeight (3);         //grosor lineas
  
  //Rectangulo superior (Acelerometro)
  fill(240);
  line(0, 100, 800, 100);
  line(0, 250, 800, 250);
  rect(0, 100, 800, 150);      
  
  //Cuadrado izquierdo (Acelerador)
  fill(0, 255, 0);
  rect(180, 500, 150, 150);  
  
  //Cuadrado derecho (Freno)
  fill(245,121,121);
  rect(450, 500, 140, 150);    
  
  //Rectangulo inferior (Luz de frenado)
  fill(250);
  //fill(252,8,8);
  rect(270, 320, 250, 100);    
  
  //Linea (indicadora velocidad)
  fill(255, 0, 0);
  line(x, 100, x, 250);       

  //Cuadro acelerador
  strokeWeight (5);
  stroke(24,131,44);
  line(180, 500, 180, 650); //Linea izq
  line(330, 500, 330, 650); //Linea der
  line(180, 500, 330, 500); //Linea sup
  line(180, 650, 330, 650); //Linea inf

  //Cuadro desacelerador
  strokeWeight (5);
  stroke(203,21,21);
  line(450, 500, 450, 650); //Linea izq
  line(590, 500, 590, 650); //Linea der
  line(450, 500, 590, 500); //Linea sup
  line(450, 650, 590, 650); //Linea inf

  //Lineas indicadoras
  strokeWeight (3);
  stroke(255, 0, 0);
  line(20, 100, 20, 120);
  line(80, 100, 80, 140); 
  line(140, 100, 140, 120);
  line(200, 100, 200, 140);
  line(260, 100, 260, 120);
  line(320, 100, 320, 140);
  line(380, 100, 380, 120);
  line(440, 100, 440, 140);
  line(500, 100, 500, 120);
  line(560, 100, 560, 140);
  line(620, 100, 620, 120);
  line(680, 100, 680, 140);
  line(740, 100, 740, 120);
  line(790, 100, 790, 140);
  
  //Valores lineas indicadoras
  textFont(createFont("Times new Roman", 20));
  fill(0);
  text("Valores en Km/h", 10, 55);
  text("20", 10, 90);
  text("80", 70, 90);
  text("140", 125, 90);
  text("200", 187, 90);
  text("260", 244, 90);
  text("320", 304, 90);
  text("380", 364, 90);
  text("440", 425, 90);
  text("500", 485, 90);
  text("560", 545, 90);
  text("620", 604, 90);
  text("680", 665, 90);
  text("740", 720, 90);
  text("800", 770, 90);

  //Nombres
  textFont(createFont("Times new Roman", 40));
  text("Velocidad: "+int(x) +" Km/h", 290, 33);
  text("Acelerador",170,480);
  text("Freno",475,480);
  text("Luz de freno",294,300);


  //Aceleracion
    if ((mouseX > 180) && (mouseX < 330) && (mouseY > 500) && (mouseY < 650) && (x<=800)){
      if(x > 798.1){
      x = x-1.999;}
      
      //Cronometro
      if (y < 1){
      t = millis();}
      
      y++;
      x=x+2;
      
      //Cronometro en la interfaz
      textFont(createFont("Times new Roman", 20));
      text("Tiempo = "+(millis() - t)/1000+" [s]",10,790);
      
      //Alertas audibles y visibles luego de 10 segundos
      if(millis() - t >= 10000){
        
        if (!alerta.isPlaying()){
            alerta.play();
          }
        
       textFont(createFont("Times new Roman", 50));
       text("Han pasado 10 segundos",150,720);}
     } 
       else{
         y=0;
         t=0;
         alerta.stop();
       }

    
  
  //Desaceleración
  if (x >= 0.5) {
    if ((mouseX > 450) && (mouseX < 590) && (mouseY > 450) && (mouseY < 650) ) {
      x = x-1;
      fill(252,8,8);
      rect(270, 320, 250, 100);}
      else{
      x=x-0.5;}}
}
